# mastodons.city

Infrastructure of https://mastodons.city


```mermaid
graph TD
LB[Google LB] --> WEB{WEB Nginx}
WEB --> APP[APP Ruby/Puma]
WEB --> FILER[FILER NFS]
APP --> FILER[FILER NFS]
STREAM --> FILER[FILER NFS]
WEB --> STREAM[STREAM NodeJS WS]
APP --> DB[DB PostgreSQL]
TASK[TASK Sidekiq] --> DB[DB PostgreSQL]
STREAM --> DB[DB PostgreSQL]
APP --> MEM[MEM Redis]
TASK --> MEM[MEM Redis]
STREAM --> MEM[MEM Redis]
DB --> DBSLAVE[DBSLAVE PostgreSQL Replication]
DBSLAVE --> BAK[BAK Dump/Borg]
```